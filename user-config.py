# https://www.mediawiki.org/wiki/Manual:Pywikibot/OAuth/Wikimedia

import os

family_files["officewiki"] = "https://office.wikimedia.org/"
usernames["officewiki"]["*"] = os.environ.get("PWB_USERNAME")
password_file = "user-password.py"
