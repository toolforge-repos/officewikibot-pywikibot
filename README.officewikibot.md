Officewikibot
=============

Officewikibot is a [Pywikibot][] install customized to work with the
[Toolforge jobs framework][] and the [Toolforge Build Service][]. It is a fork
of the [pywikibot-buildservice][] project which has been customized to support
authenticating to officewiki using [bot passwords][].

Building container image
------------------------

```shell-session
$ ssh login.toolforge.org
$ become officewikibot
$ toolforge build start https://gitlab.wikimedia.org/toolforge-repos/officewikibot-pywikibot
$ toolforge build show  # Wait until build completes
```

Using container image
---------------------

```shell-session
$ toolforge envvars create PWB_USERNAME 'Officewikibot'
$ toolforge envvars create PWB_BOTPASSWORDNAME '...'
$ toolforge envvars create PWB_BOTPASSWORD '...'
$ toolforge jobs run --image tool-officewikibot/tool-officewikibot:latest \
  --command 'pwb -family:officewiki -lang:en redirect both -moves -always' \
  --schedule '@daily' \
  fix-double-redirects
```

See also
--------
* https://wikitech.wikimedia.org/wiki/Tool:Officewikibot
* https://wikitech.wikimedia.org/wiki/Help:Toolforge/Running_Pywikibot_scripts

[Pywikibot]: https://www.mediawiki.org/wiki/Special:MyLanguage/Manual:Pywikibot
[Toolforge jobs framework]: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Jobs_framework
[Toolforge Build Service]: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service
[pywikibot-buildservice]: https://gitlab.wikimedia.org/toolforge-repos/pywikibot-buildservice
[bot passwords]: https://www.mediawiki.org/wiki/Manual:Bot_passwords
